import { validateInput, shortenPublicHoliday} from './helpers';

describe('validateInput function', () => {
    test('should throw error if country is not supported', () => {
      expect(() => validateInput({ country: 'XYZ' })).toThrowError(
        'Country provided is not supported, received: XYZ'
      );
    });
  
    test('should not throw error if country is supported', () => {
      expect(() => validateInput({ country: 'GB' })).not.toThrow();
    });
  
    test('should throw error if year is not the current year', () => {
      expect(() => validateInput({ year: 2023 })).toThrowError(
        'Year provided not the current, received: 2023'
      );
    });
  
    test('should not throw error if year is the current year', () => {
      expect(() => validateInput({ year: new Date().getFullYear() })).not.toThrow();
    });
  });

describe('shortenPublicHoliday function', () => {
test('should return shortened public holiday', () => {
    const holiday = {
    date: '2024-12-25',
    localName: 'Christmas Day',
    name: 'Christmas Day',
    countryCode: 'GB',
    fixed: true,
    global: true,
    counties: null,
    launchYear: 0,
    types: ['A public holiday celebrated on December 25th.']
    };

    const shortenedHoliday = shortenPublicHoliday(holiday);

    expect(shortenedHoliday).toEqual({
    name: 'Christmas Day',
    localName: 'Christmas Day',
    date: '2024-12-25',
    });
});
});
  