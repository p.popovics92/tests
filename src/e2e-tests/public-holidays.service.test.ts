import { getListOfPublicHolidays, checkIfTodayIsPublicHoliday, getNextPublicHolidays } from '../services/public-holidays.service';

describe('Public Holidays Service', () => {

  it('should fetch list of public holidays', async () => {
    const year = 2024;
    const country = 'GB';

    const result = await getListOfPublicHolidays(year, country);

    expect(typeof global.fetch).toBe('function');
    // Add more expectations as needed
  });

  it('should fetch list of public holidays with correct structure', async () => {
    const year = 2024;
    const country = 'GB';

    // Call the function to fetch data
    const result = await getListOfPublicHolidays(year, country);

    // Assert on the response
    expect(Array.isArray(result)).toBe(true); // Ensure the response is an array

    // Ensure each item in the array has the expected structure
    result.forEach((holiday) => {
      expect(holiday).toHaveProperty('date');
      expect(holiday).toHaveProperty('localName');
      expect(holiday).toHaveProperty('name');
    });
  });
});

it('should check if today is a public holiday', async () => {
  const country = 'GB';

  const result = await checkIfTodayIsPublicHoliday(country);

  expect(typeof global.fetch).toBe('function');
  // Add more expectations as needed
});

it('should fetch next public holidays', async () => {
  const country = 'GB';

  const result = await getNextPublicHolidays(country);

  expect(typeof global.fetch).toBe('function');
  // Add more expectations as needed
});



