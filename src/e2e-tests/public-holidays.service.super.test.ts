const request = require('supertest');

describe('Public Holidays Service E2E', () => {
    it('should fetch list of public holidays', async () => {
        const year = 2024;
        const country = 'GB';
        const apiUrl = `https://date.nager.at/api/v3/PublicHolidays/${year}/${country}`;
        
        const response = await request(apiUrl).get('');

        // Assert status code
        expect(response.status).toBe(200);

        // Assert response body structure
        expect(Array.isArray(response.body)).toBe(true);
        response.body.forEach((holiday: any) => {
            expect(holiday).toHaveProperty('date');
            expect(holiday).toHaveProperty('localName');
            expect(holiday).toHaveProperty('name');
        });
    });
  
    it('should check if today is a public holiday', async () => {
        const country = 'GB';
        const apiUrl = `https://date.nager.at/api/v3/IsTodayPublicHoliday/${country}`;
        
        const response = await request(apiUrl).get('');

        // Assert status code
        expect(response.status).toBeGreaterThan(199);
        expect(response.status).toBeLessThan(205);

        // Add more assertions if needed
    });

    it('should fetch next public holidays', async () => {
        const country = 'GB';
        const apiUrl = `https://date.nager.at/api/v3/NextPublicHolidays/${country}`;
        
        const response = await request(apiUrl).get('');

        // Assert status code
        expect(response.status).toBe(200);

        // Assert response body structure
        expect(Array.isArray(response.body)).toBe(true);
        response.body.forEach((holiday: any) => {
            expect(holiday).toHaveProperty('date');
            expect(holiday).toHaveProperty('localName');
            expect(holiday).toHaveProperty('name');
        });
    });
  });