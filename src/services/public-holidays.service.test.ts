import exp from 'constants';
import { getListOfPublicHolidays, 
         checkIfTodayIsPublicHoliday, 
         getNextPublicHolidays 
    } from './public-holidays.service';
import { SUPPORTED_COUNTRIES } from '../config';
import axios, { AxiosResponse } from 'axios';


const HUNGARY_HOLIDAYS = [
    {
        name: 'Az 1848–49-es forradalom és szabadságharc kezdetének, a modern parlamentáris Magyarország megszületésének napja, Magyarország nemzeti ünnepe. Időnként március idusaként is emlegetik e napot.',
        date: '2024-03-15',
        localName: 'Március 15 Szabadságharc'
    },
    {
        name: ' Hivatalos állami ünnep az államalapítás és az államalapító I. (Szent) István király emlékére.',
        date: '2024-08-20',
        localName: 'Államalapítás'
    },
    {
        name: 'október 23. óta ez a jeles nap kettős nemzeti ünnep Magyarországon: az 1956-os forradalom kitörésének napja és a Magyar Köztársaság 1989-es kikiáltásának napja, melyet az 1990. évi XXVIII. törvény iktatott a nemzeti ünnepek sorába.',
        date: '2024-10-23',
        localName: 'Október 23 szabadságharc'
    }
];

jest.mock('axios');

describe('Should return an array of objects with local holidays', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should return an Array of objects', async () => {
    (axios.get as jest.MockedFunction<typeof axios.get>).mockResolvedValueOnce({
      data: HUNGARY_HOLIDAYS
    } as AxiosResponse); 

    const holidaysResponse = await getListOfPublicHolidays(2024, 'GB');

    expect(holidaysResponse).toEqual(HUNGARY_HOLIDAYS);
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(expect.any(String));
  });
});


describe('checkIfTodayIsPublicHoliday function', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
  
    test('should return true if today is a public holiday', async () => {
      (axios.get as jest.Mock).mockResolvedValueOnce({ status: 200 });
  
      const result = await checkIfTodayIsPublicHoliday('GB');

      expect(result).toBe(true);
      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(expect.any(String));
    });
  
    test('should return false if today is not a public holiday', async () => {
      (axios.get as jest.Mock).mockResolvedValueOnce({ status: 204 });
  
      const result = await checkIfTodayIsPublicHoliday('GB');
  
      expect(result).toBe(false);
      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(expect.any(String));
    });
  });

  describe('getNextPublicHolidays function', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
  
    test('should return an Array of objects', async () => {
      (axios.get as jest.Mock).mockResolvedValueOnce({ data: HUNGARY_HOLIDAYS });
  
      const holidaysResponse = await getNextPublicHolidays('GB');
  
      expect(holidaysResponse).toEqual(HUNGARY_HOLIDAYS);
      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(expect.any(String));
    });
  
    test('should return an empty array on error', async () => {
      (axios.get as jest.Mock).mockRejectedValueOnce(new Error('Mock error'));
  
      const holidaysResponse = await getNextPublicHolidays('GB');
  
      expect(holidaysResponse).toEqual([]);
      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(expect.any(String));
    });
  });
