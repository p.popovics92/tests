// File: integration-tests/public-holidays.integration.test.ts

import { getListOfPublicHolidays, checkIfTodayIsPublicHoliday, getNextPublicHolidays } from '../services/public-holidays.service';

describe('Public Holidays API Integration Tests', () => {
  test('should fetch list of public holidays', async () => {
    const holidays = await getListOfPublicHolidays(2024, 'GB');

    expect(holidays).toBeDefined();
    // Add assertions based on the actual response data from the API
  });

  test('should check if today is a public holiday', async () => {
    const isHoliday = await checkIfTodayIsPublicHoliday('GB');

    expect(typeof isHoliday).toBe('boolean');
    // Add assertions based on the actual response data from the API
  });

  test('should fetch next public holidays', async () => {
    const nextHolidays = await getNextPublicHolidays('GB');

    expect(nextHolidays).toBeDefined();
    // Add assertions based on the actual response data from the API
  });
});
