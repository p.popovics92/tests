module.exports = {
  // other configuration options
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.[jt]sx?$", // adjust this to match your test file naming convention
  moduleFileExtensions: ['js', 'json', 'node'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
  